using ArgParse

include("alltypes.jl")
include("read.jl")
include("scheduler.jl")
include("optimizer.jl")
include("write.jl")

# parse options from terminal
filename, filenameRes, numSteps = parseTerminal()
# read cores and machines from input file
jobs, machines = readJobsAndMachines(filename)

# make optimization for job scheduling problem with measurement of calculation time
@time begin
    actions, timeExe = makeOptimizationParallel(jobs, machines, numSteps)
end
   
# delete output file if it exists
deleteFile(filenameRes)
# write information about actions
writeAllPerformedActions(actions, filenameRes)
# write information about execution time
writeExecutionTime(timeExe, filenameRes)
