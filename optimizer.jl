include("scheduler.jl")
include("write.jl")

# function to make optimization for job scheduling problem
function makeOptimization(jobs, machines, numSteps)

    # initialization
    timeExeBest = infTime
    actionsBest = Array{Action}(undef, 0)

    # launch optimization
    for i in 1:numSteps
        # save jobs and machines to new arrays
        jobsNew = deepcopy(jobs)
        machinesNew = deepcopy(machines)
        # launch planner
        actionsCur, timeExeCur = launchPlanning(jobsNew, machinesNew)
        if timeExeCur < timeExeBest
            # save new value of time of execution and actions
            timeExeBest = timeExeCur
            actionsBest = actionsCur
        end
    end
    
    # return the best time of execution and actions
    return actionsBest, timeExeBest 

end

# function to make optimization for job scheduling problem using several processors
function makeOptimizationParallel(jobs, machines, numSteps)

    # find number of threads
    numThread = Threads.nthreads()
    # create array of action lists and array of execution time
    arrayActionLists = Array{ActionList, 1}(undef, numThread)
    arrayTimeExection = Array{Int, 1}(undef, numThread)
    # define number of steps for every thread
    numStepsThread = Int(trunc(numSteps / numThread))
    
    # launch search of optimal solution in parallel
    Threads.@threads for i in 1:numThread
        # copy array of jobs and machines
        jobsCopy = deepcopy(jobs)
        machinesCopy = deepcopy(machines)
        # create intermediate variables
        actionList = ActionList(Array{Action,1}(undef, 0))
        timeExe = noTime
        # launch search for every thread
        actions, timeExe = makeOptimization(jobsCopy, machinesCopy, numStepsThread)
        # save found actions inside array of lists
        actionList.actions = actions
        arrayActionLists[i] = actionList
        # save found execution time inside integer array
        arrayTimeExection[i] = timeExe
    end
    
    # go through all lists
    timeExeBest = infTime
    indexBest = noIndex
    for i in 1:numThread
        # find array of actions with the best time execution
        if arrayTimeExection[i] < timeExeBest
            # save index and time of execution
            timeExeBes = arrayTimeExection[i]
            indexBest = i
        end
    end
    
    # return the best time of execution and actions
    return arrayActionLists[indexBest].actions, arrayTimeExection[indexBest]

end
