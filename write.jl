include("alltypes.jl")
 
# function to delete file if it exists in file system
function deleteFile(filename)

    # check existence of file
    if isfile(filename)
        # delete file
        rm(filename)
    end
        
end
 
# function to write in file all performed actions
function writeAllPerformedActions(actions, filename)
 
    # find number of actions
    numActions = size(actions, 1)
    # open file
    file = open(filename, "a")

    # write header rows
    write(file, "Performed actions\n")
    write(file, "idMachine idJob idTask timeBegin timeEnd\n")
    # go through all actions
    for i in 1:numActions
        # convert numbers to string
        idMachineStr = string(actions[i].idMachine)
        idJobStr = string(actions[i].idJob)
        idTaskStr = string(actions[i].idTask)
        timeBeginStr = string(actions[i].timeBegin)
        timeEndStr = string(actions[i].timeEnd)
        # print information in file about action
        write(file, idMachineStr * " " * idJobStr * " " * idTaskStr * " " * timeBeginStr 
        * " " * timeEndStr * "\n") 
    end
 
    # close file
    close(file)
 
end
 
# function to write about execution time
function writeExecutionTime(timeExe, filename)
    
    # open file
    file = open(filename, "a")
    # print information about execution time
    write(file, "Execution time: " * string(timeExe) * "\n")
    # close file
    close(file)
 
end

# function to write about execution time for specified iteration
function writeIterationExecutionTime(numIter, timeExe, filename)

    # open file
    file = open(filename, "a")
    # print information about number of iteration
    write(file, "Number of iteration: " * string(numIter) * " ")
    # print information about execution time
    write(file, "Execution time: " * string(timeExe) * "\n")
    # close file
    close(file)

end
