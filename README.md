# JoSLoS

## Introduction

**JoSLoS** (Job-shop Scheduling Lottery Solver) is software to find optimal
solution to job-shop scheduling problem using lottery scheduling approach \[0\].

## Usage

To run this software, you must enter the following command inside the project 
directory:

```bash
export JULIA_NUM_THREADS=<numThreads>; julia joslos.jl -i <inputFile> -o <outputFile> -n <numberSteps>.
```

Here, **\<numThreads\>** (default value is 1) is number of thread that will be used for parallel computing, **\<inputFile\>** (default value is "jobs.txt") and **\<outputFile\>** (default value is "results.txt") are 
names of input and output files, respectively. **\<numberSteps\>** (default value is 10000) should contain 
the number of steps that will be performed to optimize job-shop scheduling problem. 

The input file should be formed as follows: 

```go
<numJobs> <numMachines>
<idMachine> <timeExe> <idMachine> <timeExe> ... <idMachine> <timeExe>
<idMachine> <timeExe> <idMachine> <timeExe> ... <idMachine> <timeExe>
<idMachine> <timeExe> <idMachine> <timeExe> ... <idMachine> <timeExe>
...
<idMachine> <timeExe> <idMachine> <timeExe> ... <idMachine> <timeExe>
```

The first row should contain fields **\<numJobs\>** and **\<numMachines\>**, which specify number of jobs and 
machines for job-shop scheduling problem. After that, **\<numJobs\>** lines are needed to describe each job.
Each line of the description must contain even number of integers. Each odd number in the line sets 
the identifier of the machine on which the subtask of this job will be performed. In turn, each even 
number specifies the execution time of the subtask. All fields must be separated by only one space. 
Numbering of jobs and machines starts from zero. 

The output file will contain the execution time of all jobs according to the found optimal solution 
and table of special form to to specify the sequence of jobs for performing:

```go
idMachine idJob idTask timeBegin timeEnd
<idMachine> <idJob> <idTask> <timeBegin> <timeEnd>
<idMachine> <idJob> <idTask> <timeBegin> <timeEnd>
...
<idMachine> <idJob> <idTask> <timeBegin> <timeEnd>
```

**\<idTask\>** is id of subtask inside task with **\<idJob\>** id that was performed
by machine with **\<idMachine\>** id. The time of beginning and end for performing subtask are set by 
variables **\<timeBegin\>** and **\<timeEnd\>**, respectively.

Examples of input and output files for the given software are located in folder **examples**.

## Dependency

For execution of the given software, you need to install the **Julia** programming language interpreter, 
as well as the **ArgParse** library for parsing parameters that are transmitted through the terminal. 

## Algorithm

The algorithm of this program is described in detail in the file **algorithm/algorithm.pdf**. 

## License

This software has MIT License. The text of this license is located below. 

Copyright (c) 2022 Artem Tomilo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Reference

[0] Carl A. Waldspurger and William E. Weihl. 1994. Lottery scheduling: 
flexible proportional-share resource management. In Proceedings of the 
1st USENIX conference on Operating Systems Design and Implementation (OSDI '94). 
USENIX Association, USA, 1–es.
