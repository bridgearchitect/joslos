include("alltypes.jl")

# function to define rules of parsing for terminal
function defineRulesParseTerminal()

    # define rules
    rules = ArgParseSettings()
    @add_arg_table! rules begin
        "-i"
            help = "name of input file"
            default = defaultInputFilename
        "-o"
            help = "name of output file"
            default = defaultOutputFilename
        "-n"
            help = "number of steps for optimization"
            arg_type = Int
            default = defaultNumberSteps
    end
    # return rules
    return parse_args(rules)
    
end

# function to parse options from terminal
function parseTerminal()

    # initialzation
    filename = noString
    filenameRes = noString
    numSteps = noNumber
    
    # parse data from terminal
    parsedArgs = defineRulesParseTerminal()  
    # save parsed data 
    for (arg,val) in parsedArgs
        if arg == "i"
            filename = val
        elseif arg == "o"
            filenameRes = val
        elseif arg == "n"
            numSteps = Int(val)
        end
    end
    
    # return parsed data
    return filename, filenameRes, numSteps
    
end

# function to read information about jobs and machines from input file
function readJobsAndMachines(filename)

    # open file
    file = open(filename, "r")
    # read the first line
    row = readline(file)
    substrings = split(row, " ")
    
    # receive number of machines and jobs for task
    numJobs = parse(Int, substrings[1])
    numMachines = parse(Int, substrings[2])
    
    # create array of jobs and machines
    machines = Array{Machine,1}(undef, numMachines)
    jobs = Array{Job,1}(undef, numJobs)
    
    # fill array of machines
    for i in 1:numMachines
        machines[i] = Machine(noJob, noTask, noTime, noTime, Array{Task}(undef,0))
    end
    
    # go through all lines of jobs
    for i in 1:numJobs
        # read line
        row = readline(file)
        substrings = split(row, " ")
        # find number of tasks for job
        numTasks = Int(size(substrings, 1) / 2)
        # create arrays to store time of execution and ids of machines
        idMachine = Array{Int,1}(undef, numTasks)
        time = Array{Int,1}(undef, numTasks)
        # go through all tasks
        for j in 1:numTasks
            # fill array
            idMachine[j] = parse(Int, substrings[2 * j - 1]) + 1
            time[j] = parse(Int, substrings[2 * j]) + 1
        end
        # save job in special array
        jobs[i] = Job(idMachine, time, initTask)
    end
    
    # close file
    close(file)
    # return received jobs and machines
    return jobs, machines
    
end
