# define structures

# structure to describe job
mutable struct Job
    idMachine::Array{Int,1}
    time::Array{Int,1}
    curTask::Int
end

# structure to describe task
mutable struct Task
    idJob::Int
    time::Int
    prob::Float64
    idTask::Int
end

# structure to describe computing machine
mutable struct Machine
    idJob::Int
    idTask::Int
    timeFinish::Int
    timeBegin::Int
    queue::Array{Task,1}
end

# structure to describe performed action
mutable struct Action
    idMachine::Int 
    idJob::Int 
    idTask::Int 
    timeBegin::Int 
    timeEnd::Int
end

# structure to describe list of actions
mutable struct ActionList
    actions::Array{Action,1}
end

# define constants

# string constants
noString = ""
defaultInputFilename = "jobs.txt"
defaultOutputFilename = "results.txt"
defaultNumberSteps = 10000

# integer constants
noJob = -1
noTask = -1
noTime = -1
noIndex = -1
noMachine = -1
noNumber = -1
initTask = 1
zeroProb = 0.0
zeroTime = 0
infTime = 1000000000
