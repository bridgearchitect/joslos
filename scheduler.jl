include("alltypes.jl")
include("write.jl")

# function delete the first current task from job 
function deleteTaskFromJob(jobs, index)

    # check whether deleting is possible
    if size(jobs[index].time, 1) >= 1
        # delete task from schedule
        deleteat!(jobs[index].time, 1)
        deleteat!(jobs[index].idMachine, 1)
        # increase index of current task
        jobs[index].curTask = jobs[index].curTask + 1
    end
        
end

# function to create array of performed actions
function createActions()

    # create array of actions
    actions = Array{Action}(undef, 0)
    # return array of actions
    return actions
    
end

# function to create queues for every machine
function createQueues(jobs, machines)
    
    # find out number of jobs
    numJobs = size(jobs, 1)
    # go through all jobs
    for i in 1:numJobs
        # check whether job has tasks
        if size(jobs[i].time, 1) >= 1
            # receive id of machine and time of execution
            currTime = jobs[i].time[1]
            currId = jobs[i].idMachine[1]
            # add to queue of the corresponding machine
            append!(machines[currId].queue, [Task(i, currTime, zeroProb, jobs[i].curTask)])
            # delete task from job array
            deleteTaskFromJob(jobs, i)
        end
    end

end

# function to save died task as action
function saveDiedTaskAsAction(actions, machines, index, timeEnd)

    # create action
    action = Action(noMachine, noJob, noTask, noTime, noTime)
    # save fields of action
    action.idMachine = index
    action.idJob = machines[index].idJob
    action.idTask = machines[index].idTask
    action.timeBegin = machines[index].timeBegin
    action.timeEnd = timeEnd
    # add action to array
    append!(actions, [action])
    
end

# function to determine probabilities for machine tasks
function determineProbabilities(machines)

    # find out number of machines
    numMachines = size(machines, 1)
    # go through all machines
    for i in 1:numMachines
        # find out number of tasks
        numTasks = size(machines[i].queue, 1)
        # calculate inversed total time for concrete machine
        invTotalTime = 0.0
        for j in 1:numTasks
            invTotalTime += 1.0 / Float64(machines[i].queue[j].time)
        end
        # go through all tasks
        for j in 1:numTasks
            # determine new probability for task
            invTime = 1.0 / Float64(machines[i].queue[j].time)
            machines[i].queue[j].prob = invTime / invTotalTime
        end
    end
    
end

# function to choose initial tasks for machines
function chooseInitialTasks(machines)

    # find out number of machines
    numMachines = size(machines, 1)
    # go through all machines
    for i in 1:numMachines
        # find out number of tasks in queue
        numTasks = size(machines[i].queue, 1)
        # check whether machine has tasks
        if numTasks == 0
            # disable machine
            machines[i].timeBegin = noTime
            machines[i].timeFinish = noTime
            machines[i].idTask = noTask
            machines[i].idJob = noJob
            # go to new machine if machine does not have task
            continue
        end
        # launch loop to choose new task for machine
        while true
            # choose randomly task from queue
            randNum = (abs(rand(Int)) % numTasks) + 1
            randFloatVal = rand(Float64)
            if randFloatVal < machines[i].queue[randNum].prob
                # save new task for execution
                machines[i].timeFinish = machines[i].queue[randNum].time
                machines[i].timeBegin = zeroTime
                machines[i].idTask = machines[i].queue[randNum].idTask
                machines[i].idJob = machines[i].queue[randNum].idJob
                # delete task from queue
                deleteat!(machines[i].queue, randNum)
                # exit loop
                break
            end
        end
    end
    
end

# function to find the fastest task among all machines
function findFastestTask(machines)

    # find out number of machines
    numMachines = size(machines, 1)
    minTime = infTime
    minIndex = noIndex
    # go through all machines
    for i in 1:numMachines
        # find the fastest task
        if (machines[i].timeFinish < minTime) && (machines[i].idJob != noJob)
            minTime = machines[i].timeFinish
            minIndex = i
        end
    end
    
    # return found time and minimal index
    return minTime, minIndex
    
end

# function to reduce time for all machines
function reduceTimeMachines(machines, time)

    # find out number of machines
    numMachines = size(machines, 1)
    # go through all machines
    for i in 1:numMachines
        # reduce time
        if (machines[i].idJob != noJob)
            machines[i].timeFinish = machines[i].timeFinish - time
        end
    end
    
end

# function to delete task from specified machine
function deleteTaskMachine(actions, machines, index, timeEnd)
    
    # find out job id for removed task
    idJobRemoved = machines[index].idJob
    # save information about died task as action
    saveDiedTaskAsAction(actions, machines, index, timeEnd)
    # delete task from machine
    machines[index].idJob = noJob
    machines[index].idTask = noTask
    machines[index].timeFinish = noTime
    machines[index].timeBegin = noTime
    # return job id for removed task
    return idJobRemoved

end

# function to insert new task for specified machine
function insertTaskMachine(jobs, machines, idJob)
    
    # initialization
    idInsMachine = noMachine
    # check whether job has tasks
    if size(jobs[idJob].time, 1) >= 1
        # add new task to machine from this job
        time = jobs[idJob].time[1]
        idInsMachine = jobs[idJob].idMachine[1]
        append!(machines[idInsMachine].queue, [Task(idJob, time, zeroProb, jobs[idJob].curTask)])
        # delete task from job array
        deleteTaskFromJob(jobs, idJob)
    end
    # return id of machine for insering
    return idInsMachine
    
end

# function to choose new tasks for all machines
function chooseNewTasks(machines, timeBegin)

    # find out number of machines
    numMachines = size(machines, 1)
    # go through all machines
    for i in 1:numMachines
        # find out number of tasks in queue
        numTasks = size(machines[i].queue, 1)
        # check whether machine does not have tasks or machine is disabled
        if (numTasks == 0) || (machines[i].idJob != noJob)
            # go to new machine
            continue
        end
        # launch loop to choose new task for machine
        while true
            # choose randomly task from queue
            randNum = (abs(rand(Int)) % numTasks) + 1
            randFloatVal = rand(Float64)
            if randFloatVal < machines[i].queue[randNum].prob
                # save new task for execution
                machines[i].timeFinish = machines[i].queue[randNum].time
                machines[i].timeBegin = timeBegin
                machines[i].idTask = machines[i].queue[randNum].idTask
                machines[i].idJob = machines[i].queue[randNum].idJob
                # delete task from queue
                deleteat!(machines[i].queue, randNum)
                # exit loop
                break
            end
        end
    end

end

# function to determine probabilities of tasks for specified machine
function determineProbabilitiesMachine(machines, index)

    # find out number of tasks
    numTasks = size(machines[index].queue, 1)
    # check whether machine has tasks
    if numTasks == 0
        # finish working
        return
    end
    
    # calculate inversed total time for concrete machine
    invTotalTime = 0.0
    for j in 1:numTasks
        invTotalTime += 1.0 / Float64(machines[index].queue[j].time)
    end
    # go through all tasks
    for j in 1:numTasks
        # determine new probability for task
        invTime = 1.0 / Float64(machines[index].queue[j].time)
        machines[index].queue[j].prob = invTime / invTotalTime
    end

end

# function to check whether all machines are disabled
function checkDisableMachines(machines)

    # initialization
    indDis = true
    numMachines = size(machines, 1)
    # check whether all machines are disabled
    for i in 1:numMachines
        if machines[i].idJob != noJob
            indDis = false
            break
        end
    end
    # return indicator
    return indDis
    
end

# function to launch planning
function launchPlanning(jobs, machines)

    # create array of actions
    actions = createActions()
    # create queues for every machine 
    createQueues(jobs, machines)
    # determine probabilities of tasks
    determineProbabilities(machines)
    # choose tasks for execution
    chooseInitialTasks(machines)
    # determine probabilities of tasks for new task
    determineProbabilities(machines)
    
    # launch array of execution
    timeExe = 0
    while true
    
        # find the smallest time to the next task among all machines
        minTime, minIndex = findFastestTask(machines)
        # reduce time for all machines
        reduceTimeMachines(machines, minTime)
        
        # delete the found task from specified machine
        idJobRemoved = deleteTaskMachine(actions, machines, minIndex, timeExe + minTime)
        # insert new task for specified machine
        idInsMachine = insertTaskMachine(jobs, machines, idJobRemoved)
        # determine new probabilities for specified machines (removing and insering)
        determineProbabilitiesMachine(machines, minIndex)
        if idInsMachine != noMachine
            determineProbabilitiesMachine(machines, idInsMachine)
        end
        
        # choose new tasks for all machines
        chooseNewTasks(machines, timeExe + minTime)
        # determine probabilities of tasks for new task
        determineProbabilities(machines)
        
        # add new value to simulation time
        timeExe = timeExe + minTime
        # debug code
        # println(machines)
        # println(jobs)
        # check condition to exit loop
        if checkDisableMachines(machines)
            break
        end
        
    end
    
    # debug code
    # println(machines)
    # println(jobs)
    # println(timeSim)
    
    # return array of actions and time of execution
    return actions, timeExe
    
end
