\documentclass{articleSimple}

\usepackage{hyperref} % for URLs in the bibliography
\usepackage{amsmath,amssymb,bm,mathrsfs}
\usepackage{listings}
\usepackage{caption}
\usepackage{float}
\usepackage{cite}
\renewcommand{\vec}[1]{\bm{#1}}

\newcommand{\RNum}[1]{\uppercase\expandafter{\romannumeral #1\relax}}

\let\belowcaptionskip\undefined
\newlength{\belowcaptionskip}
\setlength{\belowcaptionskip}{12pt}

\begin{document}
	
	\title{Lottery Approach for Job-shop Scheduling Problem}
	\author{A. Tomilo\affiliationmark{1}}
	\affiliation{%
		\affiliationmark{1}Technical University of Dresden\\}
	\email{artem.tomilo@mailbox.tu-dresden.de}
	\maketitle

\section{Introduction}

The job-shop scheduling problem is relevant task in the development of 
operating systems (OS), because many processes (containers of computational tasks) are launched inside OS and the scheduler must distribute the execution of these processes between the several CPU cores. The difficulty of real scheduling in comparison with the model problem is that the OS scheduler must make a satisfactory decision in a limited time without complete information (e.g. time of the process execution and the dependence of starting one process on the work of another process)~\cite{Tannenbaum15}.

Several algorithms have been created for the given problem, namely the FIFO (first in -- first out) algorithm, priority queues or SJF (shortest job first) approach, each of which has its advantages and disadvantages~\cite{Tannenbaum15}. One such algorithm is lottery scheduling, which has a stochastic principle of operation in comparison with counterparts~\cite{Waldspurger94}. In this regard, it solves the problem of starvation, ie provides access to computer resources for all processes. The application of this algorithm in modern Unix-like operating systems such as Linux or OpenBSD has been actively studied~\cite{Petrou99, Mejia15}.

Due to stochasticity, the algorithm can produce different results on the same sets of processes with multiple launches. So, it can be used as part of heuristic algorithm (stochastic exploration or local search) to find solution to model problem. The purpose of this paper is to demonstrate that the adaptation of the lottery approach to the model job-shop scheduling problem is possible and can bring satisfactory results  in practice.

\section{Lottery scheduling}

Lottery scheduling is stochastic scheduling approach for processes in an operating system. Processes (computational tasks) received some number of lottery tickets. The probability of choosing this process depends on the number of its tickets (ie the more tickets mean greater probability). If the computer has multiple computing modules (for instance, multicore CPU), it is possible to use multiple queues for each module with its own tasks and the corresponding number of tickets. 

In this case, the scheduler works according to the following algorithm~\cite{Waldspurger94}:

\begin{enumerate}
\item Scheduler is waiting for the first completed process (computational task) among all computing modules.
\item The foregoing task is removed from execution by scheduler.
\item New task from the corresponding array of job is added to the queue for the
machine. In the general case, this task can be added to the queue of any computing module. The chosen task should be deleted from job array.
\item After steps 2 and 3, one or two queues will change. This will depend on whether the deleted and added tasks will belong to the same or different queues. The number of tickets must be recalculated for these changed queues.
\item One computing module does not work due to task completion. The scheduler must randomly select a new task from the corresponding queue using lottery tickets to be performed by the computing module. The selected task is removed from the queue.
\item The number of tickets is recalculated again by scheduler for the computing module where the new task was selected.
\item The last step is to move to the first step of the algorithm in order to cyclically continue the work of the scheduler.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\textwidth]{blockDiagramLottery.png} \\
	\caption{Block diagram of the lottery scheduler. This algorithm includes a loop, each iteration of which begins after the completion of task on one of the computing modules. Five different actions are performed within the loop to delete an old task and add a new task, as well as to schedule tasks that are in the queue.}
	\label{fig:blockDiagramLottery}
\end{figure}

The foregoing algorithm is almost complete. It does not include the case when the queue of computing module where the task was deleted has no new tasks. Then, this computing module will not work until new task appears in the queue. In our situation, it is also important that the scheduler should complete its work if all tasks are completed, because the theoretical model problem has a finite number of tasks in contrast to the operating system, where they can be added indefinitely. The block diagram of lottery scheduler is shown in Fig.~\ref{fig:blockDiagramLottery}.

During the implementation of scheduler, there is a problem of ticket distribution between processes in the queue. Here, the heuristic approach will be used, which has the largest number of tickets for the lowest execution time. In other words, the number of tickets depends directly on the inversed execution time $1 / t_{\text{ex}}$.
The given approach will help increase the number of completed tasks per unit of time because tasks with less execution time are more likely to run~\cite{Tannenbaum15}. This is visually shown in Fig~\ref{fig:exampleSJF}.

Also, offered modification of lottery scheduler can be considered as a stochastic version of SJF algorithm, where the process, which has the shortest execution time, will be started first~\cite{Akhtar15}. The main difference between lottery scheduler and the given counterpart is that the process with the shortest execution time will only be most likely to be run first, but other tasks may also be run first.

\newpage

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{exampleSJF.png} \\
	\caption{Example of scheduling processes without sorting (left) and with sorting (right) by time. Here the time is specified in seconds. If we have time sorting, then the computing module performs two tasks in the first eight seconds, namely B and C. Otherwise, the module calculates only one task A, ie twice less. The figure was taken from the following source~\cite{Tannenbaum15}.}
	\label{fig:exampleSJF}
\end{figure}

Example of ticket calculation code for the tasks of specific computing module is shown below. The code is written using the Julia programming language and calculates the probability of launch for task during new iteration of the scheduler instead of the ticket number. This helps avoid the problem of overflowing the memory cell, as the probability is limited in the interval $[0,1]$.

\begin{lstlisting}[language=bash]
# function to determine probabilities of tasks for specified machine
function determineProbabilitiesMachine(machines, index)

	# find out number of tasks
	numTasks = size(machines[index].queue, 1)
	# calculate inversed total time for concrete machine
	invTotalTime = 0.0
	for j in 1:numTasks
		invTotalTime += 1.0 / Float64(machines[index].queue[j].time)
	end
	
	# go through all tasks
	for j in 1:numTasks
		# determine new probability for task
		invTime = 1.0 / Float64(machines[index].queue[j].time)
		machines[index].queue[j].prob = invTime / invTotalTime
	end

end
\end{lstlisting}

\section{Algorithm adaptation}

Each new task during the iteration of the lottery scheduling is chosen randomly. So, each new launch of the given algorithm on the same data set will lead to different results, because we cannot accurately predict its work.

Due to this reason, we can use it as part of heuristic algorithm to find the optimal solution to job-shop scheduling problem. Repeated launches of the lottery algorithm and selection of the best solution among the generated variants is the key idea of this heuristic approach. The proposed concept is a modification of stochastic search, where each new instance of random solution is generated by lottery scheduling~\cite{Lehre18}.

The heuristic algorithm for finding the optimal solution of the job-shop scheduling problem is as follows:

\begin{enumerate}
	\item The first step is to launch the lottery scheduler on the given data set and obtain new instance of the solution.
	\item If this is the first launch of lottery scheduler, then the new generated instance will save as the best current solution. Otherwise, the new generated instance should be compared to the current best solution. If the new instance has a better execution time, the old solution is replaced by the new one.
	\item One is added to the loop iterator variable due to the completion of the main part of the loop.
	\item If the iterator variable has not reached the critical value, that was specified by the user, then we move on to the first step. Otherwise, the algorithm terminates.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\textwidth]{blockDiagramHeuristic.png} \\
	\caption{Block diagram of the heuristic approach to find optimal solution for job-shop scheduling problem. The algorithm consists of a loop where each iteration generates a new instance of the solution. Then new candidate is compared with the current best solution. If the new instance is better, it will be saved as the current best solution.}
	\label{fig:blockDiagramHeuristic}
\end{figure}

The block diagram of proposed heuristic algorithm is demonstrated in Fig.~\ref{fig:blockDiagramHeuristic}. Also, it can be easily parallelized, because each part of the iterations can be performed by the corresponding core of CPU, and then chose the best solution among series of several current best solutions.

\section{Verification}

The software package \texttt{JoSLoS} (Job-shop Scheduling Lottery Solver) using Julia programming language was developed to implement the foregoing algorithm to find the optimal solution to the job-shop scheduling problem. 
 
Two publications, which have examples of datasets for the job-shop scheduling problem, were used to determine the suitability of the developed algorithm~\cite{Adams88,Taillard99}. The received  answers from the written program were compared with the tabular data contained in these articles. The results are shown in Table~\ref{tab:results}. 

The number of random generated solutions was $60000$, and the program was launched in parallel ($6$ CPU cores were used) to speed up calculations. The received results are quite suitable because they are not more than $10~\%$ greater in comparison with the reference values.   

\begin{table}[H]
	\captionsetup{justification=centering}
	\begin{center}
		\begin{tabular}{||c c c||} 
			\hline
			Dataset & Time of article optimal solution & Time of found optimal solution \\ [0.5ex] 
			\hline\hline
			\texttt{abz5} & 1239 & 1280 \\
			\hline
			\texttt{abz8} & 774 & 797 \\
			\hline
			\texttt{abz9} & 751 & 807 \\
			\hline
			\texttt{ta80} & 5181 & 5687 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Comparison of the written program according to the developed algorithm, which is based on lottery scheduling, with the known results of scientific publications~\cite{Adams88,Taillard99}. The found time is close to the reference values (not more than $10~\%$). The smallest difference in values is observed on the dataset \texttt{abz8}, which is less than $3~\%$.}
	\label{tab:results}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\textwidth]{plotCPU.png} \\
	\caption{Dependence of the acceleration factor on the number of used CPU cores for the proposed algorithm of solving job-shop scheduling problem. The number of random generated solutions for search is $60000$, and the calculations were performed on the CPU AMD Ryzen 5 4600H. The obtained function is almost linear, which shows the high efficiency of software parallelization. The small nonlinearity in the use of five or six cores is explained by Amdal's law.}
	\label{fig:plotCPU}
\end{figure}

In addition, the series of launches for dataset \texttt{ta80} were performed to determine the dependence of the acceleration factor on the number of used CPU cores. The results of launches are shown in Fig.~\ref{fig:plotCPU}. The calculations were performed on a six-core CPU AMD Ryzen 5 4600H. The calculated function is almost linear, which confirms the efficiency of parallelization for the proposed algorithm. 


\section{Acknowledgement}

A. Tomilo thanks L. Alvarez, V. Reusch and K. Lautenschlager for valuable discussion about job-shop scheduling problem.

\bibliographystyle{ieeetr}
\bibliography{source.bib} 

\end{document}